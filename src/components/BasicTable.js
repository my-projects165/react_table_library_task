import React from "react";
import { useTable, useExpanded } from "react-table";
import { DATA } from "../BalanceSummary";

function Table({ columns: userColumns, data }) {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable(
      {
        columns: userColumns,
        data,
      },
      useExpanded
    );

  return (
    <>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()}>{column.render("Header")}</th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}
function App() {
  const getSubAccounts = (subRows) => {
    const result = [];
    subRows.map((obj) => {
      if (result.indexOf(obj.subaccount) === -1) {
        result.push(obj.subaccount);
      }
    });
    return result;
  };
  const getWalletDetails = (subrows) => {
    const result = [];
    const wallets = [];
    subrows.map((obj) => {
      if (wallets.indexOf(obj.account_type) === -1) {
        wallets.push(obj.account_type);
      }
    });

    wallets.map((wallet) => {
      const walletDetail = {
        exchange: wallet,
      };

      subrows.map((obj) => {
        if (obj.account_type === wallet) {
          walletDetail[obj.token + "balance"] = obj.balance;
        }
      });
      result.push(walletDetail);
    });
    return result;
  };

  const assetsArray = [];
  const exchangeArray = [];
  DATA.map((obj) => {
    if (assetsArray.indexOf(obj.token) === -1) {
      assetsArray.push(obj.token);
    }
  });

  DATA.map((obj) => {
    if (exchangeArray.indexOf(obj.exchange) === -1) {
      exchangeArray.push(obj.exchange);
    }
  });

  const mainObj = [];
  exchangeArray.map((obj) => {
    mainObj.push({
      exchange: obj,
      subRows: [],
    });
  });

  mainObj.map((obj) => {
    DATA.map((subObj) => {
      if (obj.exchange === subObj.exchange) {
        obj.subRows.push(subObj);
      }
    });
  });

  const main_obj_2 = [];
  mainObj.map((obj) => {
    const subAccounts = getSubAccounts(obj.subRows);
    const returnObj = {
      exchange: obj.exchange,
      subRows: subAccounts.map((subAccount) => {
        return {
          exchange: subAccount,
          subRows: obj.subRows.filter((subObj) => {
            return subAccount === subObj.subaccount;
          }),
        };
      }),
    };
    main_obj_2.push(returnObj);
  });

  const main_obj_3 = main_obj_2.map((venueObj) => {
    venueObj.subRows.map((accountObj) => {
      accountObj.subRows = getWalletDetails(accountObj.subRows);
    });
    return venueObj;
  });
  const columns = React.useMemo(() => [
    {
      id: "expander",
      Header: ({ getToggleAllRowsExpandedProps, isAllRowsExpanded }) => (
        <span {...getToggleAllRowsExpandedProps()}>
          {isAllRowsExpanded ? "▼" : "➤"}
        </span>
      ),

      cell: ({ row }) =>
        row.canExpand ? (
          <span
            {...row.getToggleRowExpandedProps({
              style: {
                paddingLeft: `${row.depth * 2}rem`,
              },
            })}
          >
            {row.isExpanded ? "▼" : "➤"}
          </span>
        ) : null,
    },

    {
      Header: "Venue_Name",
      accessor: "exchange",
    },

    {
      Header: "All Assets",

      columns: assetsArray.map((obj) => {
        return {
          Header: obj,
          accessor: obj + "balance",
        };
      }),
    },
  ]);

  return (
    <styles>
      <Table columns={columns} data={main_obj_3} />
    </styles>
  );
}

export default App;
